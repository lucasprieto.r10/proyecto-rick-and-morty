//
//  ViewController.swift
//  prueba
//
//  Created by Bootcamp 3 on 2022-11-03.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var buscarrick: UISearchBar!
    @IBOutlet weak var tablarick: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tablarick.delegate = self
        tablarick.dataSource = self
    }


}


extension  ViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tablarick.dequeueReusableCell(withIdentifier: "celda", for: indexPath)
        celda.textLabel?.text = "hola"
        
        
        return celda
    }
    
    
}
